# Jupyter CI

This repository demonstrates automated verification of a [Jupyter](http://jupyter.org/) notebook. Whenever the `.ipynb` file is updated it is executed by the [GitLab CI](https://about.gitlab.com/gitlab-ci/) service. Any errors are automatically emailed to the author and clearly indicated by the following badge: [![build status](https://gitlab.com/mwoodbri/jupyter-ci/badges/master/build.svg)](https://gitlab.com/mwoodbri/jupyter-ci/commits/master)

The provided example is a lightly modified version of the "Alignment viewing and filtering" recipe from [The scikit-bio cookbook](http://nbviewer.jupyter.org/github/biocore/scikit-bio-cookbook/blob/master/Index.ipynb). The [rendered version](http://nbviewer.jupyter.org/urls/gitlab.com/mwoodbri/jupyter-ci/raw/master/Alignment%20viewing%20and%20filtering.ipynb) is available via nbviewer.

To use your own notebook:

1. Clone this repository
2. Replace the notebook
3. Add any additional dependencies to `requirements.txt`. Note that the [SciPy Stack](https://www.scipy.org/stackspec.html#stackspec) is provided by default.
3. Check in your changes and push to GitLab

You'll then receive an email indicating whether the notebook could be executed and the badge will update accordingly.

This approach was inspired by an [investigation of data science reproducibility](https://markwoodbridge.com/2017/03/05/jupyter-reproducible-science.html) for Open Data Day 2017.
